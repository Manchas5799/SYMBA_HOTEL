//CREACION DE APP
const express = require('express');
const config = require('./server/config.js');

//CONFIGURACIONES
const app = config(express());
const server = app.listen(app.get('port'), () => {
    console.log("Servidor iniciado ", app.get('port'));
});
const io = require('socket.io')(server);
io.on('connection', (socket) => {
    socket.on('chat message', (msg) => {
        io.emit('chat message', {
            tipo: 1,
            contenido: msg
        });
    });
    socket.on('user_image', function (image) {
        io.sockets.emit('addimage', 'Imagen compartida', image);
    });
    socket.on('user_writing', (msg1) => {
        socket.broadcast.emit('user_writing', msg1);
    });

});