const bd = require('../server/bd');
const env = require('../config/var');
class Main {
    constructor(table_name = "", datos = "", condition = "", datos_soli = "*", tables_adi = "") {
        this.table_name = table_name;
        this.datos = datos;
        this.condition = condition;
        this.datos_soli = datos_soli;
        this.tables_adi = tables_adi;
    }
    /**
     * MSSQL TYPES : { sql.NVarchar , sql.Int, sql.Bit , sql.DateTime, sql.VarBinary, sql.TVP }
     */
    async guardar() {
        let data = null;
        if (env.development.DB == 'mysql') {
            let sql = `INSERT INTO ${this.table_name} SET ?`;
            console.log(this.datos);
            try {
                let li = await bd.query(sql, [this.datos]);
                data = li.insertId;
            } catch (error) {
                console.log("errror" + error);
                throw error;
            }
            // const data = await bd.query(sql,[this.datos]).catch((e)=>{
            //     if(e){
            //         console.log("ACCION INVALIDA EN LA BD");
            //         console.log(e);
            //         ans = e;
            //     }

            // });
        } else if (env.development.DB == 'mssql') {
            //const request = new bd.Request();
            const keys = Object.keys(this.datos);
            let sql = `INSERT INTO ${this.table_name}(`;
            let sqlValues = `VALUES(`;
            for (let key = 0; key < keys.length; key++) {
                sql += `${keys[key]}`;
                sqlValues += `${this.datos[keys[key]]}`;
                if (key != keys.length - 1) {
                    sql += ` , `;
                    sqlValues += ` , `;
                } else {
                    sql += ` ) `;
                    sqlValues += ` ) `;
                }
            }
            bd.connect().then(async res => {
                res.query(sql + sqlValues)
                    .then(data => {
                        console.log("query result --> ", data);
                    })
                    .catch(err => {
                        console.log("query error =>>>", err);
                    });
            })
        }


        return data;
    }
    async getOne() {
        let data = null;
        if (env.development.DB == 'mysql') {
            let sql = `SELECT ${this.datos_soli} FROM ${this.table_name} WHERE ?`;
            console.log(sql);
            const datos = await bd.query(sql, [this.condition]).catch((e) => {
                if (e) {
                    console.log("ACCION INVALIDA EN LA BD");
                    return null;
                }
            });
            console.log(datos);
            data = datos[0];
        } else if (env.development.DB == 'mssql') {
            //console.log(this.condition);
            const keys = Object.keys(this.condition);
            let sql = `SELECT ${this.datos_soli} FROM SISHOT.${this.table_name} WHERE `;
            let sqlValues = ``;
            for (let key = 0; key < keys.length; key++) {
            sqlValues += `${keys[key]} = ${this.condition[keys[key]]}`;
            if (key != keys.length - 1) {
                    sqlValues += ` , `;
                }
            }
            //console.log(sql+sqlValues);
            await bd.connect().then(async res => {
                let ans = await res.query(sql+sqlValues);
                data = ans.recordset[0];
                //console.log(data);
                res.close();
            });
        }
        return data;
    }
    async actualizar() {
        let sql = `UPDATE ${this.table_name} set ? WHERE ?`;
        console.log(sql);
        const datos = await bd.query(sql, [this.datos, this.condition]).catch((e) => {
            if (e) {
                console.log("ACCION INVALIDA EN LA BD");
                return null;
            }
        });
        console.log(datos);
    }
    async getAll() {
        let data = null;
        if (env.development.DB == 'mysql') {
            let sql = `SELECT ${this.datos_soli} FROM ${this.table_name}`;
            if (this.condition != '') {
                sql += ' WHERE ?';
            }
            console.log(sql);
            data = await bd.query(sql, [this.condition]).catch((e) => {
                if (e) {
                    console.log("ACCION INVALIDA EN LA BD");
                    return null;
                }
            });
            console.log(data);
        } else if (env.development.DB == 'mssql') {
            let sql = `SELECT ${this.datos_soli} FROM SISHOT.${this.table_name}`;
            await bd.connect().then(async res => {
                let ans = await res.query(sql);
                data = ans.recordset;
                res.close();
            });
        }
        return data;
    }
    // generarQuery(com){

    // }
    async eliminar() {
        let sql = `DELETE FROM ${this.table_name} WHERE ?`;
        const data = await bd.query(sql, [this.condition]).catch((e) => {
            if (e) {
                console.log("ACCION INVALIDA EN LA BD");
                return null;
            }
        });

        console.log(data);
        return data;
    }
    async inner() {
        let sql = `SELECT ${this.datos_soli} FROM ${this.table_name} as tabla1 INNER JOIN ${this.tables_adi} as tabla2 WHERE ${this.condition}`;
        console.log(sql);
        const data = await bd.query(sql).catch((e) => {
            if (e) {
                console.log("ACCION INVALIDA EN LA BD");
                return null;
            }
        });
        console.log(data);
        return data;
    }
    async filter() {
        let sql = `SELECT ${this.datos_soli} FROM ${this.table_name} WHERE ${this.condition[1]} like '%${this.condition[0]}%'`;
        console.log(sql);
        const data = await bd.query(sql).catch((e) => {
            if (e) {
                console.log("ACCION INVALIDA EN LA BD");
                return null;
            }
        });
        console.log(data);
        return data;
    }
}
module.exports = Main;