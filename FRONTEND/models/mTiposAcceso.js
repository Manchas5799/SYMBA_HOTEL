const main = require('./')
class TiposAccesos {
    constructor(datos = "") {
        //super('tipos_acceso',datos,condition,datos_soli,tables_adi);
        this.table_name = 'TIPOS_ACCESO';
        this.id = datos.id;
        this.acceso = datos.acceso;
    }
    async getAll() {
        let datos = await new main(this.table_name).getAll();
        let objs = [];
        if (datos)
            for (let i = 0; i < datos.length; i++) {
                objs[i] = new TiposAccesos({
                    id: datos[i].id,
                    acceso: datos[i].acceso
                });
            }
        return objs;
    }
    async getOne() {
        let con = new main(this.table_name, {}, { id: this.id });
        let dato = await con.getOne();
        this.setId(dato.id);
        this.setAcceso(dato.acceso);
    }
    async eliminar() {
        let con = new main(this.table_name, {}, { id: this.id });
        await con.eliminar();
    }
    async actualizar(datos) {
        let con = new main(this.table_name, datos, { id: this.id });
        await con.actualizar();
    }
    setId(id) {
        this.id = id;
    }
    setAcceso(acceso) {
        this.acceso = acceso;
    }
    async guardar() {
        return await new main(this.table_name, {
            acceso: this.acceso
        }).guardar();
    }
}

module.exports = TiposAccesos;