const main = require('.');
const habitacion = require('./mHabitacion');
const estado = require('./mEstadosHabitaciones');
const reserva = require('./mReserva');
class DetalleReserva {
	constructor(datos = {}) {
		//super('detalle_reservacion',datos,condition,datos_soli,tables_adi);
		this.table_name = 'DETALLE_RESERVACION';
		if (datos.old == null) {
			this.id = datos.id;
			this.fecha_inicio = datos.fecha_inicio;
			this.fecha_fin = datos.fecha_fin;
			this.dias = datos.dias;
			this.subTotal = datos.subTotal;
			this.habitacion = datos.HABITACIONES_id ? new habitacion({ id: datos.HABITACIONES_id }) : null;
			this.estado = datos.estados_id ? new estado({ id: datos.estados_id }) : null;
			this.reserva = datos.RESERVACION_id ? new reserva({ id: datos.RESERVACION_id }) : null;
		}else{
			this.id = datos.old.id;
			this.fecha_inicio = datos.old.fecha_inicio;
			this.fecha_fin =  datos.old.fecha_fin;
			this.dias = datos.old.dias;
			this.subTotal = datos.old.subTotal;
			this.habitacion = datos.old.habitacion;
			this.estado = datos.old.estado;
			this.reserva = datos.old.reserva;
		}

	}
	async getAll() {
		let datos = await new main(this.table_name).getAll();
		let objs = [];
		for (let i = 0; i < datos.length; i++) {
			objs[i] = new DetalleReserva({
				id: datos[i].id,
				fecha_inicio: datos[i].fecha_inicio,
				fecha_fin: datos[i].fecha_fin,
				dias: datos[i].dias,
				subTotal: datos[i].subTotal,
				HABITACIONES_id: datos[i].HABITACIONES_id,
				estados_id: datos[i].estados_id,
				RESERVACION_id: datos[i].RESERVACION_id
			});
			await objs[i].getOne();
		}
		return objs;
	}
	async getOne() {
		let con = new main(this.table_name, {}, { id: this.id });
		let dato = await con.getOne();

		this.setFechaInicio(dato.fecha_inicio);
		this.setFechaFin(dato.fecha_fin);
		this.setDias(dato.dias);
		this.setSubTotal(dato.subTotal);
		await this.setHabitaciones(dato.HABITACIONES_id);
		await this.setEstados(dato.estados_id);
		await this.setReserva(dato.RESERVACION_id);
	}
	setFechaFin(x) {
		this.fecha_fin = x;
	}
	setFechaInicio(x) {
		this.fecha_inicio = x;
	}
	setDias(x) {
		this.dias = x;
	}
	setSubTotal(x) {
		this.subTotal = x;
	}
	async setHabitaciones(id) {
		this.habitacion = new habitacion({ id });
		await this.habitacion.getOne();
	}
	async setEstados(id) {
		this.estado = new estado({ id });
		await this.estado.getOne();
	}
	async setReserva(id) {
		this.reserva = new reserva({ id });
		await this.reserva.getOne();
	}
	async guardar() {
		await new main(this.table_name, {
			fecha_inicio: this.fecha_inicio,
			fecha_fin: this.fecha_fin,
			dias: this.dias,
			subTotal: this.subTotal,
			HABITACIONES_id: this.habitacion.id,
			estados_id: 1,
			RESERVACION_id: this.reserva.id
		}).guardar();
	}
	
}

module.exports = DetalleReserva;