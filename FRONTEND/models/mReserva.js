const main = require('./');
const cliente = require('./mClientes');

class Reserva {
	constructor(datos = "") {
		//super('reservacion',datos,condition,datos_soli,tables_adi);
		this.table_name = 'RESERVACION';
		if(datos.oldDatos != null){
			this.id = datos.oldDatos.id;
			this.persona_contacto = datos.oldDatos.persona_contacto;
			this.fechaReservacion = datos.oldDatos.fechaReservacion;
			this.cliente = datos.oldDatos.cliente;
			this.detalles = datos.oldDatos.detalles;
			this.totalDetalles = datos.oldDatos.totalDetalles;	
		}else{
			this.id = datos.id;
			this.persona_contacto = datos.persona_contacto;
			this.fechaReservacion = datos.fechaReservacion;
			this.cliente = datos.CLIENTES_PERSONA_id ? new cliente({ PERSONA_id: datos.CLIENTES_PERSONA_id }) : null;
			this.detalles = [];
			this.totalDetalles = 0;
		}
	}
	async getAll() {
		let datos = await new main(this.table_name).getAll();
		let objs = [];
		for (let i = 0; i < datos.length; i++) {
			objs[i] = new Reserva({
				id: datos[i].id,
				persona_contacto: datos[i].persona_contacto,
				fechaReservacion: datos[i].fechaReservacion,
				CLIENTES_PERSONA_id: datos[i].CLIENTES_PERSONA_id
			});
			await objs[i].getOne();
		}
		return objs;
	}
	async getOne() {
		let con = new main(this.table_name, {}, { id: this.id });
		let dato = await con.getOne();

		this.setPersonaContacto(dato.persona_contacto);
		this.setFechaReservacion(dato.fechaReservacion);
		await this.setCliente(dato.CLIENTES_PERSONA_id);
	}
	setPersonaContacto(x) {
		this.persona_contacto = x;
	}
	setFechaReservacion(x) {
		this.fechaReservacion = x;
	}
	async setCliente(id) {
		console.log(id);
		this.cliente = new cliente({ PERSONA_id : id });
		await this.cliente.getOne();
	}
	addDetalle(detalle) {
		this.detalles.push(detalle);
		this.totalDetalles++;
	}
	removerDetalle(id){
		this.detalles.splice(id,1);
		this.totalDetalles--;
	}
	// verificarExistencia(id){
	// 	for(let i = 0;i < this.totalDetalles;i++){
	// 		if(this.detalles[i]){

	// 		}
	// 	}
	// }
	setId(id){
		this.id = id;
	}
	async guardar() {
		console.log(Detalle);
		let con = new main(this.table_name, {
			fechaReservacion: this.fechaReservacion,
			persona_contacto: this.persona_contacto,
			CLIENTES_PERSONA_id: this.cliente.persona.id
		});
		this.id = await con.guardar();
		for (let i = 0; i < this.totalDetalles; i++) {
			// console.log(this.detalles[i]);
			this.detalles[i] = new Detalle({old: this.detalles[i]});
			await this.detalles[i].setReserva(this.id);
			await this.detalles[i].guardar();
		}
		return this.id;
	}
	reset(){
		this.totalDetalles = 0;
		this.detalles = [];
	}
	async inner(x){
		//SELECT * FROM `reservacion` as res, `detalle_reservacion` as det_res 
		//WHERE res.CLIENTES_PERSONA_id = 14 AND det_res.RESERVACION_id = res.id
		return await new main(this.table_name,{},"tabla1.CLIENTES_PERSONA_id = " +x+ " AND tabla2.RESERVACION_id = tabla1.id","*","detalle_reservacion").inner();
	}
}

module.exports = Reserva;
const Detalle = require('./mDetalleReserva');