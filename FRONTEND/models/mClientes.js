const main = require('./');
const Usuario = require('./mUsuario');
class Cliente {
    constructor(datos = "") {
        //super('clientes',datos,condition,datos_soli,tables_adi);
        this.table_name = 'CLIENTES';
        this.fechaReg = datos.fechaReg;
        this.persona = datos.PERSONA_id ? new Usuario({ id: datos.PERSONA_id }) : null;
    }
    async guardar() {
        await new main(this.table_name, { PERSONA_id: this.persona.id }).guardar();
    }
    async inner() {
        return await new main(this.table_name, {}, "tabla2.id = tabla1.persona_id", "*", "PERSONA").inner();
    }
    async getOne(){
        let con = new main(this.table_name,{},{PERSONA_id : this.persona.id});
        console.log(this);
        let data = await con.getOne();
        this.setFechaReg(data.fechaReg);
        await this.setPerson();
    }
    async setPerson() {
		await this.persona.getOne();
    }
    setFechaReg(x){
        this.setFechaReg = x;
    }
}

module.exports = Cliente;