const main = require('./');
const TipoAcceso = require('./mTiposAcceso');
class Area {
    constructor(datos = "") {
        //super('area',datos,condition,datos_soli,tables_adi);
        this.table_name = 'AREA';
        this.id = datos.id;
        this.nombre = datos.nombre;
        this.tipoAcceso = datos.TIPOS_ACCESO_id ? new TipoAcceso({ id: datos.TIPOS_ACCESO_id }) : 0;
    }
    async getAll() {
        let datos = await new main(this.table_name).getAll();
        let objs = [];
        for (let i = 0; i < datos.length; i++) {
            objs[i] = new Area({
                id: datos[i].id,
                nombre: datos[i].nombre,
                TIPOS_ACCESO_id: datos[i].TIPOS_ACCESO_id
            });
            await objs[i].getOne();
        }
        return objs;
    }
    async getOne() {
        let con = new main(this.table_name, {}, { id: this.id });
        let dato = await con.getOne();
        this.setId(dato.id);
        this.setNombre(dato.nombre);
        this.setTipoAcceso(dato.TIPOS_ACCESO_id);
    }
    async eliminar() {
        let con = new main(this.table_name, {}, { id: this.id });
        await con.eliminar();
    }
    async actualizar(datos) {
        let con = new main(this.table_name, datos, { id: this.id });
        await con.actualizar();
    }
    setId(id) {
        this.id = id;
    }
    setNombre(nombre) {
        this.nombre = nombre;
    }
    async setTipoAcceso(id) {
        this.tipoAcceso = new TipoAcceso({ id });
        await this.tipoAcceso.getOne();
    }
    async guardar() {
        return await new main(this.table_name, {
            nombre: this.nombre,
            TIPOS_ACCESO_id: this.tipoAcceso.id

        }).guardar();
    }
}

module.exports = Area;