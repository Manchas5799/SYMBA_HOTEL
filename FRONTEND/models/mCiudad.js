const main = require('./');
const pais = require('./mPais');
class Ciudad {
    constructor(datos = "") {
        //super('ciudad',datos,condition,datos_soli,tables_adi);
        this.table_name = 'CIUDAD';
        this.id = datos.id;
        this.nombre = datos.nombre;
        this.pais = datos.PAIS_id ? new pais({ id: datos.PAIS_id }) : 0;
    }
    async getAll() {
        let datos = await new main(this.table_name).getAll();
        let objs = [];
        for (let i = 0; i < datos.length; i++) {
            objs[i] = new Ciudad({
                id: datos[i].id,
                nombre: datos[i].nombre,
                PAIS_id: datos[i].PAIS_id
            });
        }
        return objs;
    }
    async getPorPais(pais) {
        let datos = await new main(this.table_name, {}, {
            "PAIS_id": pais
        }).getAll();
        let objs = [];
        for (let i = 0; i < datos.length; i++) {
            objs[i] = new Ciudad({
                id: datos[i].id,
                nombre: datos[i].nombre,
                PAIS_id: datos[i].PAIS_id
            });
            await objs[i].getOne();
        }
        return objs;
    }
    async getOne() {
        let con = new main(this.table_name, {}, { id: this.id });
        let dato = await con.getOne();
        await this.setPais_id(dato.PAIS_id);
        this.setNombre(dato.nombre);
    }
    async eliminar() {
        let con = new main(this.table_name, {}, { id: this.id });
        await con.eliminar();
    }
    async actualizar(datos) {
        let con = new main(this.table_name, datos, { id: this.id });
        await con.actualizar();
    }
    setId(id) {
        this.id = id;
    }
    setNombre(nombre) {
        this.nombre = nombre;
    }
    async setPais_id(id) {
        this.pais = new pais({ id });
        await this.pais.getOne();
    }
    getId() {
        return this.id;
    }
    getPais() {
        return this.pais.id;
    }
    async guardar() {
        return await new main(this.table_name, {
            nombre: this.nombre,
            PAIS_id: this.pais.id
        }).guardar();
    }

}

module.exports = Ciudad;