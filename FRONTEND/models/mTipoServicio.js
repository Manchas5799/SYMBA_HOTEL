const main = require('./')
class TipoServicio {
    constructor(datos = "") {
        //super('servicio',datos,condition,datos_soli,tables_adi);
        this.table_name = 'SERVICIO';
        this.id = datos.id;
        this.nombre = datos.nombre;
        this.precio = datos.precio;
    }
    async guardar() {
        return await new main(this.table_name, {
            nombre: this.nombre,
            precio: this.precio
        }).guardar();
    }
    setId(id) {
        this.id = id;
    }
    setNombre(nombre) {
        this.nombre = nombre;
    }
    setPrecio(precio) {
        this.precio = precio;
    }
    async getAll() {
        let datos = await new main(this.table_name).getAll();
        let objs = [];
        for (let i = 0; i < datos.length; i++) {
            objs[i] = new TipoServicio({
                id: datos[i].id,
                nombre: datos[i].nombre,
                precio: datos[i].precio
            });
        }
        return objs;
    }
    async getOne() {
        let con = new main(this.table_name, {}, { id: this.id });
        let dato = await con.getOne();
        this.setPrecio(dato.precio);
        this.setNombre(dato.nombre);
    }
    async eliminar() {
        let con = new main(this.table_name, {}, { id: this.id });
        await con.eliminar();
    }
    async actualizar(datos) {
        let con = new main(this.table_name, datos, { id: this.id });
        await con.actualizar();
    }

}

module.exports = TipoServicio;