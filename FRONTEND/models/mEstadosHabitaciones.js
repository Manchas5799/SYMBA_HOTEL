const main = require('./')
class EstadosHabitaciones {
    constructor(datos = "") {
        //super('estados_habitaciones',datos,condition,datos_soli,tables_adi);
        this.table_name = 'ESTADOS_HABITACIONES';
        this.nombre = datos.nombre;
        this.id = datos.id;
    }
    async getAll() {
        let datos = await new main(this.table_name).getAll();
        let objs = [];
        if (datos)
            for (let i = 0; i < datos.length; i++) {
                objs[i] = new EstadosHabitaciones({
                    id: datos[i].id,
                    nombre: datos[i].nombre
                });
            }
        return objs;
    }
    async getOne() {
        let con = new main(this.table_name, {}, { id: this.id });
        let dato = await con.getOne();
        this.setNombre(dato.nombre);
    }
    async eliminar() {
        let con = new main(this.table_name, {}, { id: this.id });
        await con.eliminar();
    }
    async actualizar(datos) {
        let con = new main(this.table_name, datos, { id: this.id });
        await con.actualizar();
    }
    setId(id) {
        this.id = id;
    }
    setNombre(nombre) {
        this.nombre = nombre;
    }
    async guardar() {
        return await new main(this.table_name, {
            nombre: this.nombre
        }).guardar();
    }
}

module.exports = EstadosHabitaciones;