const main = require('./');
const TipoEmpleado = require('./mTipoEmpleado');
const EstadoEmpleado = require('./mEstadosEmpleados');
const Area = require('./mArea');
const Persona = require('./mUsuario');
class Empleado {
	constructor(datos = "", datosP = "") {
		//super('empleado',datos,condition,datos_soli,tables_adi);
		this.table_name = 'EMPLEADO';
		this.tipoEmpleado = datos.TIPO_EMPLEADO_id ? new TipoEmpleado({ id: datos.TIPO_EMPLEADO_id }) : 0;
		this.estado = datos.ESTADOS_id ? new EstadoEmpleado({ id: datos.ESTADOS_id }) : 0;
		this.area = datos.AREA_id ? new Area({ id: datos.AREA_id }) : 0;
		this.persona = datosP.id ? new Persona(datosP) : 0;
	}
	async getOne() {
		let datos = await new main(this.table_name, {}, `${this.persona.id} = tabla1.persona_id && tabla2.id = ${this.persona.id}`, "*", "PERSONA").inner();
		datos = datos[0];
		await this.setTipo(datos.TIPO_EMPLEADO_id);
		await this.setEstado(datos.ESTADOS_id);
		await this.setArea(datos.AREA_id);
		await this.setPerson();
	}
	async actualizar(datos, datosP) {
		let con = new main(this.table_name, datos, { PERSONA_id: this.persona.id });
		await con.actualizar();
		await this.persona.actualizar(datosP);
	}
	async eliminar() {
		let con = new main(this.table_name, {}, { PERSONA_id: this.persona.id });
		await con.eliminar();
		await this.persona.eliminar();
	}
	async setPerson() {
		await this.persona.getOne();
	}
	async setPersona() {
		let lastId = await this.persona.guardar();
		this.persona.id = lastId;
	}
	async setTipo(id) {
		this.tipoEmpleado = new TipoEmpleado({ id });
		await this.tipoEmpleado.getOne();
	}
	async setEstado(id) {
		this.estado = new EstadoEmpleado({ id });
		await this.estado.getOne();
		// this.estado.id = lastId;
	}
	async setArea(id) {
		this.area = new Area({ id });
		await this.area.getOne();
		// this.area.id = lastId;
	}
	async inner() {
		const datos = await new main(this.table_name, {}, "tabla2.id = tabla1.persona_id", "*", "PERSONA").inner();
		return datos;
	}
	async guardar() {
		await this.setPersona();
		return await new main(this.table_name, {
			TIPO_EMPLEADO_id: this.tipoEmpleado.id,
			ESTADOS_id: this.estado.id,
			AREA_id: this.area.id,
			PERSONA_id: this.persona.id
		}).guardar();
	}
}

module.exports = Empleado;