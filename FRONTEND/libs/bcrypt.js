const bC = require('bcryptjs');

const help = {};

help.encryptPassword = async (password) => {
	const salt = await bC.genSalt(10);
	const pass = await bC.hash(password,salt);
	return pass;
};
help.checkPassword = async(password,sPassword) =>{
	console.log(password,sPassword);
	const dd = await bC.compare(password,sPassword);
	console.log(dd);
	return dd;
}

module.exports = help;