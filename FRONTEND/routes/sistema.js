const router = require('express').Router();
const sistema = require('../controllers/Sistema');

function validate(req,res,next){
	if(req.isAuthenticated()){
		return next();
	}else{
		res.redirect('/login');
	}
}
/******LOGIN PARA INICIAR SESION DENTRO DEL SISTEMA**/
	
///RUTAS
router.get('/',sistema.index);

// router.get('/ingresar',sistema.mIngreso);
// router.post('/ingresar',sistema.vIngreso);

// router.get('/registrar', sistema.mRegistro);
// router.post('/registrar', sistema.vRegistro);

//POSIBLES DUDAS Y ELIMINACIONES ??? repetido??
router.get('/inventario', sistema.mInventario);
///CRUD DE  OBJETOS A MI INVENTARIO
router.get('/inventario/add', sistema.addInventario);
router.post('/inventario/add',sistema.saveInventario);

router.get('/inventario/:id',sistema.mArticulo);
router.post('/inventario/:id',sistema.updArticulo);
router.post('/inventario/delete/:id', sistema.delArticulo);

router.post('/inventario/filtrar/:filtro', sistema.fInventario);

////CRUD DE HABITACIONES
//POSIBLES DUDAS Y ELIMINACIONES ??? repetido??
router.get('/habitaciones', sistema.mHabitaciones);
router.get('/habitaciones/add', sistema.addHabitaciones);
router.post('/habitaciones/add', sistema.saveHabitaciones);

router.get('/habitaciones/:id', sistema.mHabitacion);
router.post('/habitaciones/:id', sistema.updHabitacion);
router.get('/habitaciones/delete/:id', sistema.delHabitacion);
router.post('/habitaciones/filtrar/:filtro', sistema.fHabitaciones);
///CRUD DE SERVICIOS
// router.get('/servicios', sistema.mServicios);
// router.get('/servicios/add', sistema.addServicios);
// router.post('/servicios/add', sistema.saveServicios);

// router.get('/servicios/:id', sistema.mServicios);
// router.post('/servicios/:id', sistema.updServicios);
// router.post('/servicios/delete/:id', sistema.delServicios);
// router.get('/servicios/filtrar/:filtro', sistema.fServicios);
// ///CRUD EMPLEADOS
router.get('/empleados', sistema.Empleados);
router.get('/empleados/add', sistema.addEmpleados);
router.post('/empleados/add', sistema.saveEmpleados);
router.get('/empleados/:id', sistema.mEmpleados);
router.post('/empleados/:id', sistema.updEmpleados);
router.get('/empleados/delete/:id', sistema.delEmpleados);
router.get('/empleados/filtrar/:filtro', sistema.fEmpleados);
//////REVISAR CLIENTES

// router.get('/clientes/:filtro',sistema.fClientes);




//////////////
////RUTAS DEL SISTEMA
////////
router.get('/configuracion/tipoDocumento',sistema.tipoDocumento);
router.get('/configuracion/tipoDocumento/add',sistema.addTipoDocumento);
router.post('/configuracion/tipoDocumento/add',sistema.saveTipoDocumento);
router.get('/configuracion/tipoDocumento/:id',sistema.mTipoDocumento);
router.post('/configuracion/tipoDocumento/:id',sistema.updTipoDocumento);
router.get('/configuracion/tipoDocumento/delete/:id',sistema.delTipoDocumento);
////////////
////RUTAS tipoServicio
router.get('/configuracion/tipoServicio',sistema.tipoServicio);
router.get('/configuracion/tipoServicio/add',sistema.addTipoServicio);
router.post('/configuracion/tipoServicio/add',sistema.saveTipoServicio);
router.get('/configuracion/tipoServicio/:id',sistema.mTipoServicio);
router.post('/configuracion/tipoServicio/:id',sistema.updTipoServicio);
router.get('/configuracion/tipoServicio/delete/:id',sistema.delTipoServicio);
////////////
////RUTAS tipoHabitacion
router.get('/configuracion/tipoHabitacion',sistema.tipoHabitacion);
router.get('/configuracion/tipoHabitacion/add',sistema.addTipoHabitacion);
router.post('/configuracion/tipoHabitacion/add',sistema.saveTipoHabitacion);
router.get('/configuracion/tipoHabitacion/:id',sistema.mTipoHabitacion);
router.post('/configuracion/tipoHabitacion/:id',sistema.updTipoHabitacion);
router.get('/configuracion/tipoHabitacion/delete/:id',sistema.delTipoHabitacion);
///////////
////RUTAS tipoEmpleado
router.get('/configuracion/tipoEmpleado',sistema.tipoEmpleado);
router.get('/configuracion/tipoEmpleado/add',sistema.addTipoEmpleado);
router.post('/configuracion/tipoEmpleado/add',sistema.saveTipoEmpleado);
router.get('/configuracion/tipoEmpleado/:id',sistema.mTipoEmpleado);
router.post('/configuracion/tipoEmpleado/:id',sistema.updTipoEmpleado);
router.get('/configuracion/tipoEmpleado/delete/:id',sistema.delTipoEmpleado);

////////////
////RUTAS tipoAcceso
router.get('/configuracion/tipoAcceso',sistema.tipoAcceso);
router.get('/configuracion/tipoAcceso/add',sistema.addTipoAcceso);
router.post('/configuracion/tipoAcceso/add',sistema.saveTipoAcceso);
router.get('/configuracion/tipoAcceso/:id',sistema.mTipoAcceso);
router.post('/configuracion/tipoAcceso/:id',sistema.updTipoAcceso);
router.get('/configuracion/tipoAcceso/delete/:id',sistema.delTipoAcceso);
///////////
////RUTAS hotel
router.get('/configuracion/hotel',sistema.hotel);
router.get('/configuracion/hotel/add',sistema.addHotel);
router.post('/configuracion/hotel/add',sistema.saveHotel);
router.get('/configuracion/hotel/:id',sistema.mHotel);
router.post('/configuracion/hotel/:id',sistema.updHotel);
router.get('/configuracion/hotel/delete/:id',sistema.delHotel);
///////////
////RUTAS area
router.get('/configuracion/area',sistema.area);
router.get('/configuracion/area/add',sistema.addArea);
router.post('/configuracion/area/add',sistema.saveArea);
router.get('/configuracion/area/:id',sistema.mArea);
router.post('/configuracion/area/:id',sistema.updArea);
router.get('/configuracion/area/delete/:id',sistema.delArea);

///////////
////RUTAS estadosEmpleados
router.get('/configuracion/estadosEmpleados',sistema.estadosEmpleados);
router.get('/configuracion/estadosEmpleados/add',sistema.addEstadosEmpleados);
router.post('/configuracion/estadosEmpleados/add',sistema.saveEstadosEmpleados);
router.get('/configuracion/estadosEmpleados/:id',sistema.mEstadosEmpleados);
router.post('/configuracion/estadosEmpleados/:id',sistema.updEstadosEmpleados);
router.get('/configuracion/estadosEmpleados/delete/:id',sistema.delEstadosEmpleados);

///////////
////RUTAS estadosHabitaciones
router.get('/configuracion/estadosHabitaciones',sistema.estadosHabitaciones);
router.get('/configuracion/estadosHabitaciones/add',sistema.addEstadosHabitaciones);
router.post('/configuracion/estadosHabitaciones/add',sistema.saveEstadosHabitaciones);
router.get('/configuracion/estadosHabitaciones/:id',sistema.mEstadosHabitaciones);
router.post('/configuracion/estadosHabitaciones/:id',sistema.updEstadosHabitaciones);
router.get('/configuracion/estadosHabitaciones/delete/:id',sistema.delEstadosHabitaciones);

// ///////////
// ////RUTAS estadosReservaciones
router.get('/configuracion/estadosReservaciones',sistema.estadosReservaciones);
router.get('/configuracion/estadosReservaciones/add',sistema.addEstadosReservaciones);
router.post('/configuracion/estadosReservaciones/add',sistema.saveEstadosReservaciones);
router.get('/configuracion/estadosReservaciones/:id',sistema.mEstadosReservaciones);
router.post('/configuracion/estadosReservaciones/:id',sistema.updEstadosReservaciones);
router.get('/configuracion/estadosReservaciones/delete/:id',sistema.delEstadosReservaciones);
module.exports = router;