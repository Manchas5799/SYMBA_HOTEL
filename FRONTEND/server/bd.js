const mysql = require('mysql');
const MsSql = require('mssql');
const { promisify } = require('util');
const varEnt = require('../config/var.json');
const {
    database, mssqlConf
} = require('../database/db');
let dbb = null;
if(varEnt.development.DB == 'mysql'){
    dbb = mysql.createPool(database);
    dbb.query = promisify(dbb.query);
}else if(varEnt.development.DB == 'mssql'){
    dbb = new MsSql.ConnectionPool(mssqlConf);
    
    //dbb.query = promisify(dbb.query);
    // Mssql.connect(mssqlConf).then(pool =>{
    //     if(pool.connecting){
    //         console.log("CONECTANDO A MSSQL");
    //     }
    //     if(pool.connected){
    //         console.log("CONECTADASO A MSSQL");
    //     }
    //     return pool;
    // }).catch(err=>{
    //     console.log("FALLO CONEXION A MSSQL");
    //     console.log(err);
    // });
    //const q = new MsSql.Request(dbb);
    /*MsSql.query("SELECT * FROM SISHOT.pais")
    .then(data=>{
        console.log("query result --> ", data);
    })
    .catch(err=>{
        console.log("query error =>>>", err);
    });*/
    
}
// dbb.connect().then(async res=>{
//     let data = await res.query('SELECT * FROM SISHOT.pais');
//     console.log(data);
// })
//console.log("d",new MsSql.Request(dbb).query('SELECT * FROM SISHOT.pais'));
module.exports = dbb;