const path = require('path');
const hbs = require('express-handlebars');
const morgan = require('morgan');

const varEnt = require('../config/var.json');
const flash = require('connect-flash');
// ///////////////////////////SESSION VARS
const session = require('express-session');
const sessionStore = require('express-mysql-session');/*(session);*/
// const msssqlSessionStore = require('mssql-session-store')(session);
const passport = require('passport');


//////
const { database, mssqlConf } = require('../database/db');


////
//const bd = require('../server/bd');
//////////////////////////
const multer = require('multer');
const express = require('express');
const routes = require('../routes');
// require('bootstrap');

const errorHandler = require('errorhandler');
module.exports = app => {
    ///SETTINGS
    require('../models/auth/passport');
    app.set('port', process.env.PORT || varEnt.development.PORT);
    app.set('views', path.join(__dirname, '..' + varEnt.development.VIEWS));
    app.engine('.hbs', hbs({
        defaultLayout: 'main',
        partialsDir: path.join(app.get('views'), 'partials'),
        layoutsDir: path.join(app.get('views'), 'layouts'),
        extname: '.hbs',
        helpers: require('./helpers')
    }));

    app.set('view engine', '.hbs');
    ////MIDLEWARES
    app.use(session({
        secret: 'sishotUser',
        resave: false,
        saveUninitialized: false,
        store: varEnt.development.DB == 'mysql' ? new sessionStore(database) : new msssqlSessionStore({
            connection: conect,
            ttl: 3600,
            reapInterval: 3600,
            reapCallback: function () {
                console.log('expired sessions were removed');
            }
        })
        // cookie: { secure: true }
    }));
    app.use(flash());
    app.use(morgan('dev'));
    app.use(multer({
        dest: path.join(__dirname, '../public/upload/temp')
    }).single('fileUpload'));
    app.use(express.urlencoded({
        extended: false
    }));
    app.use(express.json());
    app.use(passport.initialize());
    app.use(passport.session());

    app.use((req, res, next) => {
        /*app.locals.fallido = req.flash('fallido');
        app.locals.exitoso = req.flash('exitoso');*/
        app.locals.user = req.user;
        //app.locals.reservas = req.session.reser;
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT');
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, x-client-key, x-client-token, x-client-secret, Authorization');
        next();
    });
    ////RUTAS
    routes(app);

    ///FILES STATIC
    let pub = path.join(__dirname, '../' + varEnt.development.PUBLIC);
    // app.use('/public',express.static(path.join(__dirname,'../'+varEnt.development.PUBLIC)));
    console.log(pub);
    // console.log(path.join(__dirname,'..'+varEnt.development.PUBLIC));
    app.use('/public', express.static(path.join(__dirname, '..' + varEnt.development.PUBLIC)));
    ///ERRORHANDLERS
    if ('development' === app.get('env')) {
        app.use(errorHandler());
    }
    return app;
};